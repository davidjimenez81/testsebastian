import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http/';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(private http: HttpClient,public navCtrl: NavController) {

  }

	//Add this function and call it where you want to send it.
	sendNotification() 
	{  
	let body = {
		"notification":{
		  "title":"New Notification has arrived",
		  "body":"Notification Body",
		  "sound":"default",
		  "click_action":"FCM_PLUGIN_ACTIVITY",
		  "icon":"fcm_push_icon"
		},
		"data":{
		  "param1":"value1",
		  "param2":"value2"
		},
		  "to":"/topics/all",
		  "priority":"high",
		  "restricted_package_name":""
	  }
	  let options = new HttpHeaders().set('Content-Type','application/json');
	  this.http.post("https://fcm.googleapis.com/fcm/send",body,{
		headers: options.set('Authorization', 'key=AAAAW7q6MBw:APA91bH89S9VQFEAkhVV4bWrwmcu9IF-JjSRZDhaiFdkDrTVsBgFlMn5NE825-A6X4voJ6TggJ30QkKxuQvMyT3nkescvstI8n0y4vcYYtyUbe0vFNTbhgX-9CFZiz2ZsuJR6h5yEcY7ZV6vVflONXmiAZ1h_pdA8g'),
	  })
		.subscribe();
	}
}
